provider "aws" {
    access_key = "********"
    secret_key = "***********"
    region = "eu-west-1"
}

resource "aws_instance" "nginx_php" {
    ami = "ami-099926fbf83aa61ed"
    instance_type = "t2.micro"
    subnet_id = "subnet-3b41d161"
    key_name = "lite_win"
}

resource "aws_instance" "mysql8" {
    ami = "ami-099926fbf83aa61ed"
    instance_type = "t2.micro"
    subnet_id = "subnet-3b41d161"
    key_name = "lite_win"
}


